/** 
 * @file CoinEventHandler.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 14 de Novembro de 2016, 01:28
 */

#ifndef COINEVENTHANDLER_H
#define COINEVENTHANDLER_H

#include <cstdint>

class CoinEventHandler {
public:
    CoinEventHandler();
    virtual ~CoinEventHandler();
    void addCoinEvent(uint8_t cluster);
    void addNoneEvent();
    float getMoney() const;
    bool isLocked() const;
private:
    float getMostPopularValue();
    uint8_t lastClusters[15];
    uint8_t coinCounter = 0;
    uint8_t noCoinCounter = 0;
    float money = 0;
    bool locked = false;
    const float clustersVal[6] = {0.01f, 0.05f, 0.10f, 0.25f, 0.50f, 1.00f};
};

#endif /* COINEVENTHANDLER_H */

