/** 
 * @file MainWindow.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 15 de Outubro de 2016, 09:00
 */

#ifndef _MAINWINDOW_H
#define _MAINWINDOW_H

#include <cstdint>
#include <string>
#include <opencv2/core/core.hpp>

#include "ui_MainWindow.h"
#include "ImageQThread.h"
#include "Image.h"
#include "LVQNetwork.h"
#include "LVQTrainer.h"
#include "CoinEventHandler.h"
#include "Classifier.h"


/// GUI Main Window
class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    /// Constructor
    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();
private slots:
    
    /// Slot to update main image
    void updateImage(cv::Mat bgrMat);
    
    /// Slot to handle coin
    void onCoinDetected(std::shared_ptr<CircleImage> coin, CircleFeatures features);
    
    /// Slot to handle the acquisition of a frame without a coin
    void onNoCoin();
    
    /// Slot to update main image by a QImage
    void updateQImage(QImage image);
    
    /// Start or stop image acquisiton
    void onIniciarPausar();
    
    /// Train the neural network
    void onTrain();
    
    /// Saves the last coin as a sample image
    void onAddExemplo();
    
    /// Show about Dialog
    void showAbout();
    
    /// Opens the developer manual
    void openDevManual();
    
    /// Opens a weight file
    void openFile();
    
    /// Saves weight file
    void saveAs();
    
private:
    /// Connects signals
    void connectSignals();
    
    /// Updates features labels
    void updateFeaturesLabels(const CircleFeatures& features);
    
    /// Update total amount label
    void updateLabelMontante();
    
    /// Highlights the last detected coin
    void highlightDetectedCoin(uint8_t cluster);
    
    /// Retrieves the number of the selected cluster (radio button) or -1 (none)
    int8_t getSelectedCluster();
    
    /// Uses synthetic voice to speak the total amount of money
    void speakValue();
    
    /// Tests the neural network
    void testNetwork();
    
    /// Creates a PatternTable from a list of images
    ///
    /// Alternate path is used if path doesn't exist
    PatternTable createPatternTable(const std::string& path, 
                                    const std::string& alternatePath);
    
    /// Stylesheet used when the machine state is unlocked
    const QString stylesheetUnlocked = "QLabel {color: blue; font-size: 14pt;}";
    
    /// Stylesheet used when the machine state is locked
    const QString stylesheetLocked = "QLabel {color: red; font-size: 14pt;}";
    
    const QString commandTemplate = "spd-say --lang pt --voice female2 \"%1\"";
    
    /// Total money
    float money = 0;
    
    /// Widget collection (as designed in QtDesigner)
    Ui::MainWindow widget;
    
    /// Thread for capturing images and detecting coins
    ImageQThread imageThread;
    
    /// Learning Vector Quantization network
    LVQNetwork network;
    
    /// Coin Classifier
    Classifier classifier;

    /// List of widgets for highlighting the closest cluster
    QVector<QLabel*> clusterLabels;
    
    /// List of widgets for selecting the cluster associated with an image
    QVector<QRadioButton*> clusterRadioButtons;
    
    /// Frame as captured by webcam
    QImage frame;
    
    /// BGR image of the last coin acquired
    Image* lastCoin = nullptr;
    
    /// Event handler for coin detection machine state
    CoinEventHandler handler;
};

#endif /* _MAINWINDOW_H */
