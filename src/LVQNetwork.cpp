/** 
 * @file LVQNetwork.cpp
 * @author leandro
 *
 * @date 15 de Outubro de 2016, 00:57
 */

#include "config.h"
#include "constants.h"
#include "LVQNetwork.h"
#include "CircleImage.h"
#include "PatternTable.h"
#include <cmath>
#include <exception>
#include <iostream>
#include <cstdint>

#ifdef __ARM_NEON
#include <arm_neon.h>
#endif

LVQNetwork::LVQNetwork(uint8_t nInputs, uint8_t nOutputs) {
    this->nInputs = nInputs;
    this->nOutputs = nOutputs;
    weights = new float*[nOutputs];
    for (uint8_t j = 0; j < nOutputs; j++)
        weights[j] = new float[nInputs];
}

LVQNetwork::LVQNetwork(const LVQNetwork& orig) {
    throw new std::runtime_error("LVQ network cannot be copied!");
}


LVQNetwork::~LVQNetwork() {
    for (uint8_t j = 0; j < nOutputs; j++)
        delete [] weights[j];
    delete [] weights;
}


void LVQNetwork::initWeights(const PatternTable& initSet) {
    for (uint16_t entry = 0; entry < initSet.size(); entry++) {
        std::vector<float> inputLine = initSet[entry].first;
        const uint8_t cluster = initSet[entry].second;
        for (uint8_t i = 0; i < nInputs; i++)
            weights[cluster][i] = inputLine[i];
    }
}


uint8_t LVQNetwork::classify(std::vector<float> pattern) {
    float minDist = 0.0f;
    float currDist;
    uint8_t i, j;
    uint8_t cluster = 0;
#ifdef NEON_CLASSIFY
    float32x4_t inputNeon = vld1q_f32(pattern.data());
#endif    
    //Iterating clusters
    for (j = 0; j < nOutputs; j++) {
        currDist = 0.0f;
        //Iterating inputs
#ifdef NEON_CLASSIFY
    float32x4_t weightsNeon = vld1q_f32(weights[j]);
    float32x4_t aux = vabdq_f32(weightsNeon, inputNeon);
    aux = vmulq_f32(aux, aux);
    for (i = 0; i < nInputs; i++)
        currDist += aux[i];
#else
    for (i = 0; i < nInputs; i++)
        currDist += (weights[j][i] - pattern[i]) * (weights[j][i] - pattern[i]);
#endif
        //Finding minimal distance
        if (j == 0)
            minDist = currDist;
        else if (currDist < minDist) {
            minDist = currDist;
            cluster = j;
        }
    }
    return cluster;
}


LVQNetwork LVQNetwork::fromStream(std::istream& in, uint8_t nInputs, uint8_t nOutputs) {
    LVQNetwork network(nInputs, nOutputs);
    for (uint8_t j = 0; j < nOutputs; j++) {
        for (uint8_t i = 0; i < nInputs; i++) {
            in >> network.weights[j][i];
            if (!in) {
                throw std::runtime_error("Arquivo inválido!");
            }
        }
    }
    return network;
}


void LVQNetwork::load(std::istream& in) {
    for (uint8_t j = 0; j < nOutputs; j++) {
        for (uint8_t i = 0; i < nInputs; i++) {
            in >> weights[j][i];
            if (!in) {
                throw std::runtime_error("Arquivo inválido!");
            }
        }
    }
}


void LVQNetwork::write(std::ostream & out) {
    for (uint8_t j = 0; j < nOutputs; j++) {
        uint8_t i;
        for (i = 0; i < nInputs - 1; i++)
            out << weights[j][i] << " ";
        out << weights[j][i] << std::endl;
    }
}