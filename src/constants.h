/**
 * @file constants.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date November 7, 2016, 9:46 PM
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "config.h"

#ifdef __cplusplus
extern "C" {
#endif

namespace Paths {
constexpr auto InitSet = DATA_DIR "/init";
constexpr auto TrainSet = DATA_DIR "/train";
constexpr auto TestSet = DATA_DIR "/test";
constexpr auto UnitTestSet1 = DATA_DIR "/unittest1";
constexpr auto UnitTestSet2 = DATA_DIR "/unittest2";

    namespace Alternate {
        constexpr auto InitSet = "data/init";
        constexpr auto TrainSet = "data/train";
        constexpr auto TestSet = "data/test";
        constexpr auto UnitTestSet1 = "data/unittest1";
        constexpr auto UnitTestSet2 = "data/unittest2";
    }
}


#ifdef __cplusplus
}
#endif

#endif /* CONSTANTS_H */

