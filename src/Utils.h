/**
 * @file Utils.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date 11 de Novembro de 2016, 22:19
 */

#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <string>

/// Utility class
class Utils {
public:
    /**
     * List the filenames of a given directory, omitting . and ..
     * @param path Path to the directory
     * @param fullpath Whether full paths should be returned (default is false)
     * @param sort Whether the list should be alphabetically sorted (default is false)
     * @return A vector of filenames
     */
    static std::vector<std::string>
    listDir ( std::string path, bool fullpath = false, bool sorted = false );

    /**
     * Tests whether a file path is a regular directory
     * @param path A file path to be checked
     * @return True if the path is a regular directory; false otherwise
     */
    static bool
    isDir ( std::string path );

    /**
     * Splits the string into tokens according to the given delimiter
     * @param needle The string to be split
     * @param delimiter The char to be used as a delimiter
     * @return A vector of parts, omitting the token
     */
    static std::vector<std::string>
    split ( std::string& needle, char const* delimiter );

    /**
     * Gets the basename of a file path
     * @param filepath Absolute or relative file path
     * @return The file name
     */
    static std::string
    getBaseName ( std::string filepath );

};

#endif /* UTILS_H */

