#include <QApplication>
#include "MainWindow.h"
#include "config.h"

Q_DECLARE_METATYPE(cv::Mat)
int main(int argc, char *argv[]) {
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);
    
    QApplication app(argc, argv);
    app.setWindowIcon(QIcon("./data/qtmonnaie.png"));
    
    MainWindow win;
    win.show();

    return app.exec();
}
