/**
 * @file dumpPatternTable.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date November 29, 2016, 9:46 PM
 */

#include "config.h"
#include "constants.h"

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <cstring>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include "LVQNetwork.h"
#include "CircleImage.h"
#include "CircleFeatures.h"
#include "PatternTable.h"
#include "Utils.h"

#include <argp.h> //must appear after CircleImage.h

using std::string;
using std::vector;
 
//=========================================================================
// Argument Parsing Structures
//=========================================================================

const char *argp_program_version = "DumpTable " VERSION_MAJOR "." VERSION_MINOR;
const char *argp_program_bug_address = "<" BUG_EMAIL ">";

static char doc[] = "DumpTable --- dumps features, patterns or network response"
                    "tables from sample images in the provided directory";

static char args_doc[] = "directory";

static struct argp_option options[] = {
    {"features", 'f', 0, 0, "Write out features table"},
    {"pattern", 'p', 0, 0, "Write out LVQ Network pattern table"},
    {"network", 'n', "<weights file>", 0, "Write out LVQ Network test table"},
    {"header", '1', 0, 0, "Enable header row"},
    {0}
};

struct arguments {
    char *directory = NULL;
    char* networkWeights = nullptr;
    bool header = false;
    bool features = false;
    bool pattern = false;
};

static error_t parse_opt ( int key, char *arg, struct argp_state *state ) {
    struct arguments *arguments = ( struct arguments * ) state->input;
    switch ( key ) {
    case '1':
        arguments->header = true;
        break;
    case 'f':
        arguments->features = true;
        break;
    case 'p':
        arguments->pattern = true;
        break;
    case 'n':
        arguments->networkWeights = arg;
        break;
    case ARGP_KEY_ARG:
        if ( state->arg_num >= 1 )
            argp_usage ( state ); //too many args
        arguments->directory = arg;
        break;
    case ARGP_KEY_END:
        if ( state->arg_num < 1 )
            argp_usage ( state ); //not enough args
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

//=========================================================================
// Helper Functions
//=========================================================================

bool hasNoCluster ( string filename ) {
    return CircleImage::getCluster ( filename ) == -1;
}

void printFeatures ( CircleFeatures& features ) {
    using std::array;
    auto arr = features.toStdArray();
    for ( int i = 0; i < arr.size() - 1; i++ )
        printf ( "%hu ", arr[i] );
    printf ( "%hu\n", arr[arr.size() - 1] );
}

void printLVQInput ( vector<float>& input ) {
    printf ( "%f %f %f", input[0], input[1], input[2] );
}

//=========================================================================
// Main
//=========================================================================

/*
 *
 */
int main ( int argc, char** argv ) {
    struct arguments arguments;
    vector<string> entries;
    PatternTable pat;

    // Argument Parsing
    // -----------------

    argp_parse ( &argp, argc, argv, 0, 0, &arguments );

    bool cond1 = arguments.features;
    bool cond2 = arguments.pattern;
    bool cond3 = ( arguments.networkWeights != nullptr );

    if ( ( cond1 && cond2 ) || ( cond2 && cond3 ) || ( cond1 && cond3 ) ) {
        fprintf ( stderr, "Options --features ( -f ), --pattern ( -p ) and "
                  "--network ( -n ) mutually exclusive!" );
        abort();
        return 1;
    } else if ( !cond1 && !cond2 && !cond3 ) {
        fprintf ( stderr, "Please choose the table type with -f, -p or -n!" );
        abort();
        return 2;
    }

    // Common operations
    // -----------------

    string path = arguments.directory;
    if ( path[path.length() - 1] == '/' )
        path.pop_back();

    if ( arguments.pattern || ( arguments.networkWeights != nullptr ) ) {
        entries = Utils::listDir ( path, true, true );
        auto begin = entries.begin();
        auto end = entries.end();
        auto newEnd = std::remove_if ( begin, end, hasNoCluster );
        entries.erase ( newEnd, end );
        pat = CircleImage::createPatternTable ( entries );
    }

    // Pattern table
    //-------------
    if ( arguments.pattern ) {
        if ( arguments.header )
            std::cout << "normCr normCb normRad" << std::endl;
        pat.write ( std::cout );

    }

    // Features Table
    // --------------
    else if ( arguments.features ) {
        vector<string> entries = Utils::listDir ( path, true, true );
        if ( arguments.header )
            puts ( "filename Cr Cb Rad dCr dCb" );
        for ( string entry : entries ) {
            CircleImage img ( entry );
            CircleFeatures features = img.getFeatures();
            printf ( "%s ", entry.c_str() );
            printFeatures ( features );
        }
    }

    // LVQ Network test table
    // ----------------------
    else {
        
        std::ifstream inputFile ( arguments.networkWeights);
        LVQNetwork network ( 3,4 );
        network.load ( inputFile );
        if ( arguments.header )
            puts ( "entry cr cb radius target output" );
        for ( int e = 0; e < entries.size(); e++ ) {
            string entry = entries[e];
            vector<float> input = pat[e].first;
            uint8_t target = pat[e].second;
            uint8_t output = network.classify ( input );
            printf ( "%s ", entry.c_str() );
            printLVQInput ( input );
            printf ( " %hu %hu\n", target, output );
        }
    }
    return 0;
}
