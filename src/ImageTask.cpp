/*
 * @file ImageTask.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date November 6, 2016, 20:18 AM
 */

#include <memory>

#include "ImageTask.h"

ImageTask::ImageTask ( int deviceNo ) {
    capture = VideoCapture ( deviceNo );
}

ImageTask::~ImageTask() {
    capture.release();
}

Mat ImageTask::getLastFrame() const {
    return bgr;
}

std::shared_ptr<CircleImage> ImageTask::getLastCoin() const {
    return coinImage;
}

CircleFeatures ImageTask::getLastCoinFeatures() const {
    return coinImage->getFeatures();
}

ImageTask::FrameResult ImageTask::singleIteration() {
    if ( !capture.read ( bgr ) ) {
        return kNoImage;
    }
    cvtColor ( bgr, yCrCb, COLOR_BGR2YCrCb );
    //cvtColor(bgr, gray, COLOR_BGR2GRAY);
    split ( yCrCb, channels );
    GaussianBlur ( channels[0], blur, Size ( 9, 9 ), 2, 2 );
    std::vector<Vec3f> circles;
    HoughCircles ( blur, circles, CV_HOUGH_GRADIENT, 2, blur.rows / 8, 200, 100, 98, 160 );
    int16_t maxRadius = 0;
    int16_t coinXc, coinYc;

    bool found = false;
    for ( size_t i = 0; i < circles.size(); i++ ) {
        int16_t xc = cvRound ( circles[i][0] );
        int16_t yc = cvRound ( circles[i][1] );
        int16_t radius = cvRound ( circles[i][2] );
        if ( radius < 10 ) {
            continue;
        }
        if ( ( xc - radius ) < 0 || ( yc - radius ) < 0 ||
                ( xc + radius ) > yCrCb.cols || ( yc + radius ) > yCrCb.rows )
            continue;
        if ( radius > maxRadius ) {
            maxRadius = radius;
            coinXc = xc;
            coinYc = yc;
            found = true;
        }
    }
    if ( !found )
        return kNoCoin;
    Point center ( coinXc, coinYc );
    circle ( bgr, center, 3, Scalar ( 0, 255, 0 ), -1, 8, 0 ); //highlight center
    circle ( bgr, center, maxRadius, Scalar ( 0, 0, 255 ), 3, 8, 0 ); //outline
    CircleImage* coinImageRawPtr = new CircleImage ( yCrCb, coinXc, coinYc, maxRadius );
    coinImage = std::shared_ptr<CircleImage> ( coinImageRawPtr );
    return kCoinDetected;
}


