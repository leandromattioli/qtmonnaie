#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <iostream>

class ProgressBar {
public:
    ProgressBar(unsigned maxVal, unsigned width = 60);
    void step();
    void printBars(std::ostream& out);
    void printText(std::ostream& out);
    float getFraction();
private:
    unsigned maxVal; //!< Maximum value for the progress bar
    unsigned currVal; //!< Current value for the progress bar
    unsigned width; //!< Width for bars display
};

#endif // PROGRESSBAR_H