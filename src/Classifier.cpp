/** 
 * @file Classifier.c
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 1 de Dezembro de 2016, 20:47
 */

#include <cstdint>
#include <vector>
#include "LVQNetwork.h"
#include "Classifier.h"
#include "CircleFeatures.h"

Classifier::Classifier(LVQNetwork& net) : network(net){
}

Classifier::Classifier(const Classifier& orig) : network(orig.network) {
}

Classifier::~Classifier() {
}

int8_t Classifier::classify(CircleFeatures& features) {
    if(features.deltaAvgCr > 10 && features.deltaAvgCb > 10 &&
            inRange(features.avgCr, 100, 150) &&
            inRange(features.avgCb, 80, 150) && 
            features.radius > 50) {
        return 5;
    }
    auto inputPat = features.toLVQInput();
    int8_t cluster = network.classify(inputPat);
    return cluster + 1; //cluster 0 is moeda_005
}

bool Classifier::inRange(float val, float min, float max) {
    return(val >= min && val <= max);
}

bool Classifier::inRange(int val, int min, int max) {
    return(val >= min && val <= max);
}