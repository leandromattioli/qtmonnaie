#include "ProgressBar.h"
#include <string>
#include <iomanip>

using std::setw;
using std::setprecision;
using std::fixed;
using std::endl;

ProgressBar::ProgressBar(unsigned maxVal, unsigned width) {
    this->maxVal = maxVal;
    this->width = width;
    currVal = 0;
}

void ProgressBar::step() {
    if (currVal > maxVal)
        return;
    currVal++;
}

float ProgressBar::getFraction() {
    return (float) currVal / maxVal;
}

void ProgressBar::printText(std::ostream& out) {
    float percent = getFraction() * 100;
    out << "[" << fixed << setw(3) << setprecision(0) << percent << "%]";
    out.flush();
}

void ProgressBar::printBars(std::ostream& out) {
    out << "[";
    unsigned usedWidth = width - 2; //ignoring [ and ]
    float fraction = getFraction();
    unsigned bars = (unsigned) (fraction * usedWidth);
    unsigned i;
    for (i = 0; i < bars; i++)
        out << "#";
    for (i = bars; i < usedWidth; i++)
        out << " ";
    out << "]";
    out.flush();
}