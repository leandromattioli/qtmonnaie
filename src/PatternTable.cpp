/* 
 * @file PatternTable.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date November 6, 2016, 9:24 AM
 */

#include <cstdint>
#include <vector>
#include <utility>
#include <iostream>
#include <iomanip>

#include "PatternTable.h"

PatternTable::PatternTable() {
    inputs.reserve(32);
    outputs.reserve(32);
}

PatternTable::~PatternTable() {
}

void PatternTable::addEntry(std::vector<float> input, uint8_t output) {
    inputs.push_back(input);
    outputs.push_back(output);
}

PatternTable& PatternTable::operator+=(TableEntry entry) {
    inputs.push_back(entry.first);
    outputs.push_back(entry.second);
    return *this;
}

TableEntry PatternTable::operator[](int idx) const {
    return TableEntry(inputs[idx], outputs[idx]);
}

std::vector<float> PatternTable::getInput(uint16_t index) const {
    return inputs[index];
}

uint8_t PatternTable::getOutput(uint16_t index) const {
    return outputs[index];
}

uint16_t PatternTable::size() const {
    return (uint16_t) outputs.size();
}

void PatternTable::write(std::ostream& out) const {
    for (uint16_t e = 0; e < size(); e++) {
        std::vector<float> input = inputs[e];
        for(uint8_t i = 0; i < input.size(); i++)
            out << input[i] << " ";
        out << +outputs[e] << std::endl;
    }
}