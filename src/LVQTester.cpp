/** 
 * @file LVQTester.cpp
 * @author Leandor Mattioli <leandro.mattioli@gmail.com>
 * @date 13 de Novembro de 2016, 15:51
 */

#include "LVQTester.h"
#include "LVQNetwork.h"
#include "PatternTable.h"

LVQTester::LVQTester(LVQNetwork& _net, const PatternTable& _testSet) :
net(_net), testSet(_testSet) {
    currEntry = 0;
    wrong = 0;
}

LVQTester::~LVQTester() {
}

uint16_t LVQTester::getWrong() const {
    return wrong;
}

bool LVQTester::hasNext() const {
    return currEntry < testSet.size();
}

bool LVQTester::testSingleEntry() {
    TableEntry entry = testSet[currEntry];
    std::vector<float> input = entry.first;
    uint16_t desiredCluster = entry.second;
    uint8_t currCluster = net.classify(input);
    currEntry++;
    if (desiredCluster != currCluster) {
        wrong++;
        return false;
    }
    return true;
}
