/**
 * @file TrainLVQNetwork.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date November 30, 2016, 9:32 AM
 */

#include "config.h"

#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <argp.h>

#include "constants.h"
#include "LVQNetwork.h"
#include "LVQTrainer.h"
#include "LVQTester.h"
#include "CircleImage.h"
#include "PatternTable.h"
#include "ProgressBar.h"
#include "Utils.h"

using std::string;
using std::vector;
using std::cout;
using std::endl;

// ============================================================================
// Arguments Parsing and Usage
// ============================================================================

const char *argp_program_version = "TrainLVQ " VERSION_MAJOR "." VERSION_MINOR;
const char *argp_program_bug_address = "<" BUG_EMAIL ">";

static char doc[] = "TrainLVQ --- trains the LVQ network and dumps the "
                    "adjusted weights";

static char args_doc[] = "<alpha1> <alpha2> <epochs> <weights_output>";

struct arguments {
    float alpha1 = 0.0f;
    float alpha2 = 0.0f;
    unsigned epochs = 0;
    char* output = nullptr;
    char* error_graph = nullptr;
};

static struct argp_option options[] = {
    {"error_graph", 'e', "FILE", 0, "Writes error x time graph in FILE"},
    {0}
};

static error_t parse_opt ( int key, char *arg, struct argp_state *state ) {
    struct arguments *arguments = ( struct arguments* ) state->input;
    switch ( key ) {
    case 'e':
        arguments->error_graph = arg;
        break;
    case ARGP_KEY_ARG:
        if ( state->arg_num >= 4 )
            argp_usage ( state ); //too many args
        else if ( state->arg_num == 0 )
            arguments->alpha1 = atof ( arg );
        else if ( state->arg_num == 1 )
            arguments->alpha2 = atof ( arg );
        else if ( state->arg_num == 2 )
            arguments->epochs = atoi ( arg );
        else if ( state->arg_num == 3 )
            arguments->output = arg;
        break;
    case ARGP_KEY_END:
        if ( state->arg_num < 3 )
            argp_usage ( state ); //not enough args
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

// ============================================================================
// Main routines and functions
// ============================================================================

bool hasNoCluster ( string filename ) {
    return CircleImage::getCluster ( filename ) == -1;
}

void testNetwork ( LVQNetwork& network, vector<string>& testEntries,
                   PatternTable& testSet, FILE* graph ) {
    unsigned wrong = 0;
    for ( int e = 0; e < testEntries.size(); e++ ) {
        char cluster = network.classify ( testSet[e].first );
        if ( cluster != testSet[e].second )
            wrong++;
    }
    if( graph != nullptr) {
        float fraction = (float) wrong / testEntries.size();
        fprintf(graph, "%u %hu\n", wrong, (unsigned short) (fraction * 100));
    }
}

vector<string> getEntries(string path, string alternatePath) {
    string realPath = (Utils::isDir(path)) ? path : alternatePath;
    return Utils::listDir(realPath, true, true);
}

/*
 *
 */
int main ( int argc, char** argv ) {
    struct arguments arguments;
    argp_parse ( &argp, argc, argv, 0, 0, &arguments );

    vector<string> entries;
    vector<string> testEntries;
    
    entries = getEntries(Paths::InitSet, Paths::Alternate::InitSet);
    PatternTable initSet = CircleImage::createPatternTable ( entries );

    entries = getEntries(Paths::TrainSet, Paths::Alternate::TrainSet);
    PatternTable trainingSet = CircleImage::createPatternTable ( entries );

    testEntries = getEntries(Paths::TestSet, Paths::Alternate::TestSet);
    auto begin = testEntries.begin();
    auto end = testEntries.end();
    auto newEnd = std::remove_if ( begin, end, hasNoCluster );
    testEntries.erase ( newEnd, end );
    PatternTable testSet = CircleImage::createPatternTable ( testEntries );

    LVQNetwork network ( 3, 4 );
    LVQTrainer trainer ( network, trainingSet,
                         arguments.alpha1, arguments.alpha2, arguments.epochs );

    printf ( "Network init..." );
    network.initWeights ( initSet );
    puts ( "\r[DONE] Network init." );
    fflush ( stdout );

    puts ( "Training Parameters:" );
    printf ( "Alpha1=%f  Alpha2=%f  ", arguments.alpha1, arguments.alpha2 );
    printf ( "Epochs=%u\n", arguments.epochs );
    fflush ( stdout );
    int epoch = 1;
    ProgressBar progress ( arguments.epochs, 60 );
    FILE* errorGraph = nullptr;
    if(arguments.error_graph != nullptr)
        errorGraph = fopen(arguments.error_graph, "w");
    while ( trainer.hasNext() ) {
        progress.step();
        progress.printBars ( cout );
        cout << "\r";
        trainer.trainSingleEpoch();
        testNetwork ( network, testEntries, testSet, errorGraph);
        epoch++;
    }
    cout << endl;
    cout.flush();


    printf ( "Saving adjusted weights." );

    std::ofstream out ( arguments.output );
    network.write ( out );
    out.close();

    puts ( "\r[DONE] Saving adjusted weights." );
    fflush ( stdout );

    return 0;
}

