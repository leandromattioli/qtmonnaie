/** 
 * @file CircleImage.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 7 de Novembro de 2016, 09:38
 */

#include "config.h"
#include <utility>
#include <string>
#include <array>
#include <cstddef>
#include <cstdlib>
#include <cstdint>
#include <cmath>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "CircleImage.h"
#include "PatternTable.h"
#include "Utils.h"

#include <cstdio> //@DEBUG

using std::string;

#ifdef __ARM_NEON
#include <arm_neon.h>
#endif

CircleImage::CircleImage(cv::Mat img) {
    this->img = img;
    xc = img.cols / 2;
    yc = img.rows / 2;
    radius = xc;
    shouldReleaseImage = false;
}

CircleImage::CircleImage(cv::Mat img, int16_t xc, int16_t yc, uint16_t radius) {
    this->xc = xc;
    this->yc = yc;
    this->radius = radius;
    uint16_t diameter = radius << 1;
    cv::Rect rect = cv::Rect(xc - radius, yc - radius, diameter, diameter);
    this->img = cv::Mat(img, rect); //.clone() ??
    this->xc = this->yc = radius; //after cropping, (xc,yc) coordinates changes
    shouldReleaseImage = true;
}

CircleImage::CircleImage(std::string filePath) {
    this->img = cv::imread(filePath, cv::IMREAD_COLOR);
    xc = img.cols / 2;
    yc = img.rows / 2;
    radius = xc;
    cvtColor(this->img, this->img, cv::COLOR_BGR2YCrCb); //important!
    shouldReleaseImage = true;
}

CircleImage::~CircleImage() {
    if (shouldReleaseImage)
        this->img.release();
}

cv::Vec3b CircleImage::getAvgColor(float minNormRadius, float maxNormRadius) {
    cv::Vec3b avgColor(0, 0, 0);
    if (img.cols == 0 || img.rows == 0)
        return avgColor;

    int count = 0;
    float minRadius = minNormRadius * radius;
    float maxRadius = maxNormRadius * radius;
    int32_t minRadiusSqr = cvRound(minRadius * minRadius);
    int32_t maxRadiusSqr = cvRound(maxRadius * maxRadius);
    int32_t rowDeltaSqr;
    int32_t centerSqrDist;

#ifndef NEON_AVG_COLOR
    cv::Vec3i colorSum(0, 0, 0);
    for (uint16_t i = 0; i < img.rows; i++) {
        uint8_t* row = img.ptr<uint8_t>(i);
        rowDeltaSqr = (i - yc)*(i - yc);
        for (uint16_t j = 0; j < img.cols; j++) {
            centerSqrDist = rowDeltaSqr + (j - xc)*(j - xc);
            if ((centerSqrDist >= minRadiusSqr) &&
                    (centerSqrDist <= maxRadiusSqr)) {
                int col = 3 * j;
                colorSum[0] += row[col];
                colorSum[1] += row[col + 1];
                colorSum[2] += row[col + 2];
                count++;
            }
        }
    }
#else
    uint32x4_t colorSum = {0, 0, 0, 0};
    uint8x8_t currentColor;
    int32x2_t ij = {0, 0};
    int32x2_t center = {yc, xc};
    int32x2_t aux;
    for (ij[0] = 0; ij[0] < img.rows; ij[0]++) {
        uint8_t* row = img.ptr<uint8_t>(ij[0]);
        for (ij[1] = 0; ij[1] < img.cols; ij[1]++) {
            aux = vabd_s32(ij, center);
            aux = vmul_s32(aux, aux);
            centerSqrDist = aux[0] + aux[1];
            if ((centerSqrDist >= minRadiusSqr) &&
                    (centerSqrDist <= maxRadiusSqr)) {
                currentColor = vld1_u8(&(row[3 * ij[1]]));
                colorSum = vaddw_u16(colorSum, vget_low_u16(vmovl_u8(currentColor)));
                count++;
            }
        }
    }
#endif
    avgColor[0] = (uint8_t) (colorSum[0] / count);
    avgColor[1] = (uint8_t) (colorSum[1] / count);
    avgColor[2] = (uint8_t) (colorSum[2] / count);
    return avgColor;
}

CircleFeatures CircleImage::getFeatures() {
    CircleFeatures features;
    cv::Vec3b avgColor, avgExtRad, avgIntRad;

    avgColor = getAvgColor(0, 1);
    avgExtRad = getAvgColor(0.75f, 0.9f);
    avgIntRad = getAvgColor(0, 0.5f);

    /*Feature 1 : Mean color (Cr and Cb) 
      Feature 2 : Color difference between outer radius and inner radius    	
      Feature 3 : Diameter (image width)*/

    //Creating Feature Vector
    features.avgCr = avgColor[1];
    features.avgCb = avgColor[2];
    features.deltaAvgCr = (uint8_t) abs(avgExtRad[1] - avgIntRad[1]);
    features.deltaAvgCb = (uint8_t) abs(avgExtRad[2] - avgIntRad[2]);
    features.radius = (uint8_t) (img.cols >> 1); //radius = diameter / 2

    return features;
}

PatternTable CircleImage::createPatternTable(std::vector<std::string> images) {
    using std::string;

    PatternTable pat;
    for (string filepath : images) {
        try {
            TableEntry tableEntry = getTableEntry(filepath);
            pat += tableEntry;
        } catch (const std::runtime_error&) {
            continue;
        }
    }
    return pat;
}

cv::Mat CircleImage::getImg() const {
    return img;
}

TableEntry CircleImage::getTableEntry(std::string filename) {
    using std::array;

    int8_t cluster = getCluster(filename);
    if (cluster != -1) {
        CircleImage circle(filename);
        CircleFeatures features = circle.getFeatures();
        auto inputPattern = features.toLVQInput();
        return TableEntry(inputPattern, cluster);
    }
    throw std::runtime_error("Image does not represent any LVQ cluster!");
}

int8_t CircleImage::getCluster(std::string filename) {
    const char kClusterNames[4][10] = {
        "moeda_005",
        "moeda_010",
        "moeda_025",
        "moeda_050"
    };
    for (int j = 0; j < 4; j++) {
        if (filename.find(kClusterNames[j]) != string::npos)
            return j;
    }
    return -1;
}
