/** 
 * @file Utils.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date 11 de Novembro de 2016, 22:19
 */

#include "Utils.h"
#include <dirent.h>
#include <vector>
#include <algorithm>
#include <string>
#include <stdexcept>
#include <iostream>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>

std::vector<std::string> Utils::listDir(std::string path, bool fullpath, bool sorted) {
    std::vector<std::string> entries;
    DIR* dir = opendir(path.c_str());
    if (dir == NULL) {
        throw std::runtime_error("Invalid dir!");
    }
    struct dirent* entry;
    for (entry = readdir(dir); entry != NULL; entry = readdir(dir)) {
        if (entry->d_name[0] == '.') {
            continue;
        }
        if(!fullpath)
            entries.push_back(std::string(entry->d_name));
        else
            entries.push_back(path + "/" + std::string(entry->d_name));
    }
    closedir(dir);
    if(sorted)
        std::sort(entries.begin(), entries.end());
    return entries;
}

bool Utils::isDir ( std::string path ) {
    struct stat info;

    if(stat( path.c_str(), &info ) != 0)
        return false;
    else if(info.st_mode & S_IFDIR)
        return true;
    return false;
}


std::vector<std::string> Utils::split(std::string& needle, char const* delimiter) {
    std::vector<std::string> parts;
    char* token = strtok((char*) needle.c_str(), delimiter);
    while (token != NULL) {
        parts.push_back(std::string(token));
        token = strtok(NULL, delimiter);
    }
    return parts;
}

std::string Utils::getBaseName(std::string filepath) {
    unsigned char pos = filepath.find_last_of("/");
    if (pos == -1)
        pos = 0;
    return filepath.substr(pos + 1);
}
