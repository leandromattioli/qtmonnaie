/** 
 * @file LVQTrainer.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 15 de Outubro de 2016, 00:57
 */

#include "LVQNetwork.h"
#include "LVQTrainer.h"

LVQTrainer::LVQTrainer(LVQNetwork& _network, const PatternTable& _trainingSet,
        float initialAlpha, float finalAlpha, uint16_t maxEpochs) :
net(_network), trainingSet(_trainingSet) {
    this->initialAlpha = initialAlpha;
    this->finalAlpha = finalAlpha;
    this->maxEpochs = maxEpochs;
    alphaStep = (finalAlpha - initialAlpha) / maxEpochs;
    currAlpha = initialAlpha;
    currEpoch = 0;
}

bool LVQTrainer::trainSingleEpoch() {
    if (currEpoch > maxEpochs)
        return false;
    //Steps 0 and 1: Done outside this method
    //Step 2 : For each training input vector x...
    std::vector<float> xs;
    uint16_t j;
    uint16_t targetCluster;
    for (int t = 0; t < trainingSet.size(); t++) {
        xs = trainingSet.getInput(t);
        targetCluster = trainingSet.getOutput(t);
        //Step 3 : Find J so that | x - wj | is minimum
        j = net.classify(xs);
        //Step 4 : Update wj

        //Method 1: update winner
        //        for (uint16_t i = 0; i < xs.size(); i++)
        //            net.weights[j][i] += (uint16_t)
        //                (currAlpha * ((float) xs[i] - net.weights[j][i]));

        //Method 2: update only if winner is right
        if (j == targetCluster) {
            for (uint16_t i = 0; i < xs.size(); i++)
                net.weights[j][i] += currAlpha * (xs[i] - net.weights[j][i]);
        }
        else {
            for (uint16_t i = 0; i < xs.size(); i++)
                net.weights[j][i] -= currAlpha * (xs[i] - net.weights[j][i]);
        }
    }
    //Step 5 : Reduce learning rate
    currAlpha += alphaStep;
    //Step 6 : Stop criteria by number of epochs
    currEpoch++;
    return true;
}

bool LVQTrainer::hasNext() const {
    return currEpoch < maxEpochs;
}
