/** 
 * @file CircleFeatures.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 10 de Novembro de 2016, 10:40
 */

#include "config.h"
#include "CircleFeatures.h"
#include <array>
#include <vector>
#include <opencv2/core/core.hpp>
#include <cstdint>

#ifdef __ARM_NEON
#include <arm_neon.h>
#endif

using std::vector;

CircleFeatures::CircleFeatures() {
#ifdef NEON_NORMALIZE
    for(int i = 0; i < 3; i++)
        deltaInv[i] = 1 / (upper[i] - lower[i]); //NEON doesn't have division
#endif
}


std::array<uint8_t, 5> CircleFeatures::toStdArray() const {
    return std::array<uint8_t, 5>{
        avgCr, avgCb, radius, deltaAvgCr, deltaAvgCb};
}


std::vector<float> CircleFeatures::toLVQInput() const {
#ifndef NEON_NORMALIZE
    const uint8_t kLimits[3][2] = {
        {F_LOWER_CR, F_UPPER_CR}, //average Cr
        {F_LOWER_CB, F_UPPER_CB}, //average Cb
        {F_LOWER_RAD, F_UPPER_RAD} //radius
    };
    vector<float> pattern = {(float) avgCr, (float) avgCb, (float) radius};
    for (int i = 0; i < 3; i++) {
        uint8_t lower = kLimits[i][0];
        uint8_t delta = (float) (kLimits[i][1] - lower);
        pattern[i] = (float) (pattern[i] - lower) / delta;
    }
    return pattern;
#else
    float32x4_t pattern = {(float) avgCr, (float) avgCb, (float) radius};
    pattern = vsubq_f32(pattern, lower);
    pattern = vmulq_f32(pattern, deltaInv);
    float data[4];
    vst1q_f32(data, pattern);
    return vector<float> (data, data+4);
#endif
}