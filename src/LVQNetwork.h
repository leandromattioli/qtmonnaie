/** 
 * @file LVQNetwork.h
 * @author leandro
 *
 * @date 15 de Outubro de 2016, 00:57
 */

#ifndef LVQNETWORK_H
#define LVQNETWORK_H

#include <string>
#include <cstdint>
#include "CircleImage.h"
#include "CircleFeatures.h"

#ifdef __ARM_NEON
#include <arm_neon.h>
#endif

class LVQTrainer;

/**
 * @brief Learning Vector Quantization Network for Coins Recognition
 * @param nInputs Number of neurons on the input layer
 * @param nOutputs Number of neurons on the output layer
 */
class LVQNetwork {
        friend LVQTrainer;
public:
    
    /**
     * Constructor
     * @param nInputs Number of neurons on the input layer
     * @param nOutputs Number of neurons on the output layer
     */
    LVQNetwork(uint8_t nInputs, uint8_t nOutputs);
    
    LVQNetwork(const LVQNetwork& orig);

    
    virtual ~LVQNetwork();
    
    /// Initializes network according to a pattern table
    /// @param pat Pattern table containing one sample of each cluster
    void initWeights(const PatternTable& pat);
    
    /**
     * Classify a pattern returning the closest cluster
     * @param pattern The input pattern
     * @return The closest cluster index
     */
    uint8_t classify(std::vector<float> pattern);
    
    /**
     * Creates an LVQNetwork from an input stream (weight matrix) from a stream
     * @param in Input stream containing valid data (numbers separated by space)
     * @param nInputs Number of neurons on the input layer
     * @param nOutputs Number of neurons on the output layer
     * @return Whether the operation was performed successfully
     */
    static LVQNetwork fromStream(std::istream& in, uint8_t nInputs, uint8_t nOutputs);
    
    /**
     * Loads weight matrix (numbers separated by space) from a stream
     * @note Weights are represented with 16 bits
     * @param in Input stream
     */
    void load(std::istream& in);
    
    /**
     * Outputs weight matrix (numbers separated by space)
     * @note Weights are represented with 16 bits
     * @param out Output stream to write data
     */
    void write(std::ostream& out);
    
private:
    /// Number of input neurons
    uint8_t nInputs;
    
    /// Number of clusters
    uint8_t nOutputs;
    
    /// Transposed weight matrix (1st index: cluster ; 2nd index: input)
    float** weights;

};

#endif /* LVQNETWORK_H */

