/** 
 * @file LVQTester.h
 * @author Leandor Mattioli <leandro.mattioli@gmail.com>
 * @date 13 de Novembro de 2016, 15:51
 */

#ifndef LVQTESTER_H
#define LVQTESTER_H

#include <cstdint>

#include "LVQNetwork.h"
#include "PatternTable.h"

/**
 * Iterative tester for LVQ Networks
 * This tester is suited for use combined with a progress bar
 * @param net The associated LVQ Network
 * @param pat The truth table for the network's test set
 */
class LVQTester {
public:
    /**
     * Constructor
     * @param net The associated LVQ Network
     * @param testSet The truth table for the network's test set
     */
    LVQTester(LVQNetwork& _net, const PatternTable& _testSet);
    
    virtual ~LVQTester();
    
    /**
     * Tests a single entry and advances to the next entry
     * @return Whether the network classification is correct
     */
    bool testSingleEntry();
    
    /// Check if more entries need to be tested
    bool hasNext() const;
    
    /// Retrieves the accumulated number of wrong results
    uint16_t getWrong() const;
private:
    /// The associated LVQ Network
    LVQNetwork& net;
    
    /// Test set
    const PatternTable& testSet;
    
    /// Wrong result count during test
    uint16_t wrong;

    /// Current entry
    uint16_t currEntry;
};

#endif /* LVQTESTER_H */