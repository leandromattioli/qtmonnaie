/**
 * @file MainWindow.cc
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 15 de Outubro de 2016, 09:00
 */

#include "config.h"

#include <fstream>
#include <iostream>
#include <cstdio>

#include <QThread>
#include <QMessageBox>
#include <QErrorMessage>
#include <QDesktopServices>
#include <QUrl>
#include <QFileDialog>
#include <QProgressDialog>
#include <QDateTime>
#include <QProcess>

#include "MainWindow.h"
#include "ImageQThread.h"
#include "Image.h"
#include "CircleImage.h"
#include "PatternTable.h"
#include "LVQNetwork.h"
#include "LVQTrainer.h"
#include "Utils.h"
#include "constants.h"
#include "LVQTester.h"

using namespace Qt;
using std::vector;
using std::string;

MainWindow::MainWindow ( QWidget *parent ) : network ( 3, 4 ), classifier ( network ) {
    ( void ) parent; //make compiler happy!

    // Registering types used in signals and slots
    qRegisterMetaType< cv::Mat > ( "cv::Mat" );
    qRegisterMetaType< std::shared_ptr<CircleImage> > ( "std::shared_ptr<CircleImage>" );
    qRegisterMetaType< CircleFeatures > ( "CircleFeatures" );

    widget.setupUi ( this );

    //Preparing neural network
    try {
        PatternTable initPat = createPatternTable ( Paths::InitSet,
                               Paths::Alternate::InitSet );
        network.initWeights ( initPat );
    } catch ( const std::runtime_error& ) {
        QMessageBox::critical ( this, "Erro", "Erro ao ler exemplos de imagens" );
    }

    //Labels used for highlighting the detected coin
    clusterLabels.reserve ( 6 );
    clusterLabels.append ( widget.labelCluster0 );
    clusterLabels.append ( widget.labelCluster1 );
    clusterLabels.append ( widget.labelCluster2 );
    clusterLabels.append ( widget.labelCluster3 );
    clusterLabels.append ( widget.labelCluster4 );
    clusterLabels.append ( widget.labelCluster5 );

    //Radio buttons
    clusterRadioButtons.reserve ( 6 );
    clusterRadioButtons.append ( widget.radioCluster0 );
    clusterRadioButtons.append ( widget.radioCluster1 );
    clusterRadioButtons.append ( widget.radioCluster2 );
    clusterRadioButtons.append ( widget.radioCluster3 );
    clusterRadioButtons.append ( widget.radioCluster4 );
    clusterRadioButtons.append ( widget.radioCluster5 );

    connectSignals();
}

void MainWindow::connectSignals() {
    //Image thread (producer)
    connect ( widget.btnIniciar, &QPushButton::clicked,
              this, &MainWindow::onIniciarPausar );
    connect ( widget.btnTreinar, &QPushButton::clicked,
              this, &MainWindow::onTrain );
    connect ( widget.actionSobre, &QAction::triggered,
              this, &MainWindow::showAbout );
    connect ( widget.actionManual, &QAction::triggered,
              this, &MainWindow::openDevManual );
    connect ( widget.actionAbrir, &QAction::triggered,
              this, &MainWindow::openFile );
    connect ( widget.actionSalvar, &QAction::triggered,
              this, &MainWindow::saveAs );
    connect ( widget.btnAddExemplo, &QPushButton::clicked,
              this, &MainWindow::onAddExemplo );
    connect ( &imageThread, &ImageQThread::newFrame,
              this, &MainWindow::updateImage );
    connect ( &imageThread, &ImageQThread::newQImage,
              this, &MainWindow::updateQImage );
    connect ( &imageThread, &ImageQThread::newCoin,
              this, &MainWindow::onCoinDetected );
    connect ( &imageThread, &ImageQThread::noCoin,
              this, &MainWindow::onNoCoin );
}

MainWindow::~MainWindow() {
}

//=============================================================================
// GUI Helpers
//=============================================================================

void MainWindow::updateFeaturesLabels ( const CircleFeatures& features ) {
    widget.labelCr->setText ( QString::number ( features.avgCr, 10 ) );
    widget.labelCb->setText ( QString::number ( features.avgCb, 10 ) );
    widget.labelDeltaCr->setText ( QString::number ( features.deltaAvgCr, 10 ) );
    widget.labelDeltaCb->setText ( QString::number ( features.deltaAvgCb, 10 ) );
    widget.labelRaio->setText ( QString::number ( features.radius, 10 ) );
}

void MainWindow::updateLabelMontante() {
    float oldTotal = money;
    money = handler.getMoney();
    QString moneyStr = QString::number ( money, 'f', 2 );
    widget.labelMontante->setText ( QString ( "R$ %1" ).arg ( moneyStr ) );
    if ( handler.isLocked() )
        widget.labelMontante->setStyleSheet ( stylesheetLocked );
    else {
        widget.labelMontante->setStyleSheet ( stylesheetUnlocked );
    }
    if ( money != oldTotal )
        speakValue();
}

void MainWindow::highlightDetectedCoin ( uint8_t cluster ) {
    for ( QLabel* label : clusterLabels ) {
        label->setFrameShape ( QFrame::Shape::NoFrame );
    }
    clusterLabels[cluster]->setFrameShape ( QFrame::Shape::Box );
}

int8_t MainWindow::getSelectedCluster() {
    for ( uint8_t i = 0; i < 6; i++ ) {
        if ( clusterRadioButtons[i]->isChecked() )
            return i;
    }
    return -1;
}

void MainWindow::speakValue() {
    uint16_t cents = ( uint16_t ) ( money * 100 );
    QString message;
    if ( cents == 1 )
        message = "1 centavo";
    else if ( cents < 100 )
        message = QString ( "%1 centavos" ).arg ( QString::number ( cents ) );
    else if ( cents == 100 )
        message = "1 real";
    else if ( cents % 100 == 0 ) {
        message = QString ( "%1 reais" ).arg ( QString::number ( cents / 100 ) );
    } else {
        message = QString ( "%1 e %2" ).arg ( QString::number ( cents / 100 ),
                                              QString::number ( cents % 100 ) );
    }
    QString command = commandTemplate.arg ( message );
    std::cout << command.toStdString() << std::endl;
    system ( command.toStdString().data() );
}


//=============================================================================
//Slots
//=============================================================================

void MainWindow::updateImage ( Mat bgrMat ) {
    Image image ( bgrMat );
    frame = image.makeQImage();
    if ( !frame.isNull() )
        widget.labelFrame->setPixmap ( QPixmap::fromImage ( frame ) );
}

void MainWindow::updateQImage ( QImage qimg ) {
    if ( !qimg.isNull() )
        widget.labelFrame->setPixmap ( QPixmap::fromImage ( qimg ) );
}

void MainWindow::onCoinDetected ( std::shared_ptr<CircleImage> coin,
                                  CircleFeatures features ) {
    Mat img;
    cv::cvtColor ( coin->getImg(), img, CV_YCrCb2BGR );
    if ( lastCoin != nullptr )
        delete lastCoin;
    lastCoin = new Image ( img );
    cv::resize ( img, img, Size ( 160, 160 ) );
    Image image ( img );
    QImage qimg = image.makeQImage();
    if ( !qimg.isNull() )
        widget.labelCircle->setPixmap ( QPixmap::fromImage ( qimg ) );
    updateFeaturesLabels ( features );
    int8_t cluster = classifier.classify ( features );
    highlightDetectedCoin ( cluster );
    handler.addCoinEvent ( cluster );
    updateLabelMontante();
}

void MainWindow::onNoCoin() {
    handler.addNoneEvent();
    updateLabelMontante();
}

void MainWindow::onIniciarPausar() {
    if ( !imageThread.isRunning() ) {
        int deviceNo = widget.spinDevice->value();
        imageThread.initCapture ( deviceNo );
        imageThread.begin();
    } else {
        imageThread.terminate();
    }
}

void MainWindow::onTrain() {
    QString title = "Treinando rede...";
    float alpha1 = ( float ) widget.spinAlpha1->value();
    float alpha2 = ( float ) widget.spinAlpha2->value();
    uint16_t maxEpochs = ( uint16_t ) widget.spinEpocas->value();
    PatternTable trainingSet = createPatternTable ( Paths::TrainSet,
                               Paths::Alternate::TrainSet );
    LVQTrainer trainer ( network, trainingSet, alpha1, alpha2, maxEpochs );
    QProgressDialog progress ( title, QString(), 0, maxEpochs, this );
    progress.setWindowModality ( WindowModality::WindowModal );
    for ( uint16_t i = 0; i < maxEpochs; i++ ) {
        progress.setValue ( i );
        trainer.trainSingleEpoch();
    }
    progress.setValue ( maxEpochs );
    testNetwork();
}

void MainWindow::onAddExemplo() {
    if ( lastCoin == nullptr ) {
        QMessageBox::critical ( this, "Erro!", "Nenhuma moeda detectada!" );
        return;
    }
    int8_t cluster = getSelectedCluster();
    if ( cluster == -1 ) {
        QMessageBox::critical ( this, "Erro!", "Por favor selecione um cluster!" );
        return;
    }
    const char* prefixes[] = {
        "moeda_001",
        "moeda_005",
        "moeda_010",
        "moeda_025",
        "moeda_050",
        "moeda_100"
    };
    //From Qt documentation:
    //Formats without separators (e.g. "HHmm") are currently not supported.
    QDateTime now = QDateTime::currentDateTime();
    QString suffix = now.toString ( QString ( "yyyyMMdd_HHmmss" ) );
    QString prefix = QString ( prefixes[cluster] );
    QString filename = QString ( "%1_full_%2.bmp" ).arg ( prefix, suffix );
//     if ( !frame.isNull() ) {
//         frame.save ( filename );
//     }
    lastCoin->save(filename);
}

void MainWindow::openFile() {
    QString filename;
    filename = QFileDialog::getOpenFileName ( this,
               tr ( "Abrir arquivo de pesos" ), "./", tr ( "Arquivos de pesos (*.txt)" ) );
    if ( filename.isEmpty() )
        return;
    try {
        std::ifstream ifs ( filename.toStdString(), std::ifstream::in );
        network.load ( ifs );
    } catch ( const std::runtime_error& error ) {
        QString errorMsg = "<b>Erro ao abrir arquivo:</b><br>";
        errorMsg.append ( error.what() );
        QMessageBox::critical ( this, "Erro", errorMsg );
    }

}

void MainWindow::saveAs() {
    QString filename;
    filename = QFileDialog::getSaveFileName ( this,
               tr ( "Salvar arquivo de pesos" ), "./", tr ( "Arquivos de pesos (*.txt)" ) );
    if ( filename.isEmpty() )
        return;
    try {
        std::ofstream ofs ( filename.toStdString(), std::ofstream::out );
        network.write ( ofs );
    } catch ( const std::exception& exception ) {
        QString errorMsg = "<b>Erro ao salvar arquivo:</b><br>";
        errorMsg.append ( exception.what() );
        QMessageBox::critical ( this, "Erro", "Erro ao salvar arquivo!" );
    }
}

void MainWindow::openDevManual() {
    QString link = DOC_ROOT;
    QDesktopServices::openUrl ( QUrl ( link ) );
}

void MainWindow::showAbout() {
    QFile aboutFile ( ":/Texts/res/texts/about.html" );
    if ( !aboutFile.open ( QIODevice::ReadOnly | QIODevice::Text ) )
        QMessageBox::critical ( this, "Erro!", "Erro interno desconhecido!" );
    QString aboutFileStr = aboutFile.readAll();
    string tplVars[][2] = {
        {"@AUTHOR_NAME@", AUTHOR_NAME},
        {"@AUTHOR_EMAIL@", AUTHOR_EMAIL},
        {"@VERSION_MAJOR@", VERSION_MAJOR},
        {"@VERSION_MINOR@", VERSION_MINOR},
        {"@PROJECT_URL@", PROJECT_URL}
    };
    for ( int i = 0; i < 5; i++ ) {
        aboutFileStr = aboutFileStr.replace ( QString ( tplVars[i][0].c_str() ),
                                              QString ( tplVars[i][1].c_str() ) );
    }
    QMessageBox::about ( this, "Sobre o QtMonnaie", aboutFileStr );
}

//=============================================================================
//Other Member Functions
//=============================================================================

PatternTable MainWindow::createPatternTable ( const string& path,
        const string& alternatePath ) {
    PatternTable pat;
    auto realPath = ( Utils::isDir ( path ) ) ? path : alternatePath;
    vector<string> entries = Utils::listDir ( realPath, true, true );
    QProgressDialog progress ( "Carregando amostras...", QString(), 0, entries.size(), this );
    progress.setWindowModality ( WindowModality::WindowModal );
    for ( uint16_t e = 0; e < entries.size(); e++ ) {
        std::string entry = entries[e];
        try {
            TableEntry tableEntry = CircleImage::getTableEntry ( entry );
            pat += tableEntry;
        } catch ( std::runtime_error& ex ) {
            /* skipping invalid filenames */
        }
        progress.setValue ( e );
    }
    progress.setValue ( entries.size() );
    progress.close();
    return pat;
}

void MainWindow::testNetwork() {
    QString title = "Testando a rede...";
    PatternTable testSet = createPatternTable ( Paths::TestSet,
                           Paths::Alternate::TestSet );
    LVQTester tester ( network, testSet );
    QProgressDialog progress ( title, QString(), 0, testSet.size(), this );
    progress.setWindowModality ( WindowModality::WindowModal );
    uint16_t numEntries = testSet.size();
    for ( uint16_t i = 0; i < numEntries; i++ ) {
        progress.setValue ( i );
        tester.testSingleEntry();
    }
    progress.setValue ( numEntries );
    progress.close();
    uint16_t wrong = tester.getWrong();
    float fraction = ( float ) wrong / ( float ) numEntries;
    QString wrongStr = QString::number ( wrong );
    QString message = QString ( "Erros: <strong>%1 de %2 (%3)</strong><br>\n" );
    message = message.arg (
                  wrongStr,
                  QString::number ( numEntries ),
                  QString::number ( fraction ) );
    QMessageBox::information ( this, "Resultado", message );
}
