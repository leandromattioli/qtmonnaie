/** 
 * @file ImageQThread.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date 13 de Outubro de 2016, 20:36
 */

#ifndef IMAGEQTHREAD_H
#define IMAGEQTHREAD_H

#include <QReadWriteLock>
#include <QObject>
#include <QThread>
#include <QImage>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <memory>

#include "ImageTask.h"


using namespace cv;

/**
 * @brief Qt Thread that captures and processes images from camera
 * @param parent The parent QObject
 */
class ImageQThread : public QThread {
    Q_OBJECT
public:
    /// Constructor
    /// @param parent The parent object
    ImageQThread(QObject* parent = NULL);
    
    /// Destructor
    virtual ~ImageQThread();

    /// Opens WebCam and start acquisiton
    /// @param deviceNo The camera to use or -1 to use whatever is available
    void initCapture(int deviceNo);

    /// Starts the thread main loop
    void begin();

    /// Stops the thread execution
    void stop();

    /// Whether capture is stopped
    bool isStopped() const;
protected:
    /// Thread main loop
    void run();
signals:
    /// Signal emitted for reporting errors
    /// @param err The localized error message
    void error(QString err);

    /// Signal emitted when a new frame is acquired
    /// @param mat The processed image
    void newFrame(const cv::Mat& mat);

    /// Signal emitted when a coin is detected
    /// @param coin Cropped image showing only the detected coin
    /// @param features Features extracted from the coin image
    /// @see CircleFeatures
    void newCoin(const std::shared_ptr<CircleImage>& coin,
            const CircleFeatures& features);
    
    /// Signal emitted when a image is acquired but no coin is detected
    void noCoin();

    /// Signal emitted when a new QImage is acquired
    /// @param img The processed image
    /// @deprecated
    void newQImage(const QImage& img);

private:
    bool stopFlag;
    ImageTask* task;
    std::shared_ptr<CircleImage> coin;
};

#endif /* IMAGEQTHREAD_H */

