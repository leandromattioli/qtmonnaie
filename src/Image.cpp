/** 
 * @file Image.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 13 de Outubro de 2016, 20:33
 */

#include <QObject>
#include <QImage>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "Image.h"

Image::Image(Mat mat) {
    this->mat = mat;
}

Image::Image(QString& filename) {
    this->mat = imread(filename.toStdString(), IMREAD_COLOR);
    if(this->mat.data == NULL) {
        throw cv::Exception();
    }
}

QImage Image::makeQImage() const {
    if (mat.channels() == 3) {
        return QImage((const uint8_t*) (mat.data),
                mat.cols, mat.rows, QImage::Format_RGB888).rgbSwapped();
    } else {
        return QImage((const uint8_t*) (mat.data),
                mat.cols, mat.rows, QImage::Format_Indexed8);
    }
}

Mat Image::getMat() const {
    return mat;
};

void Image::load(QString& filename) {
    this->mat = imread(filename.toStdString(), IMREAD_COLOR);
    if(this->mat.data == NULL) {
        throw cv::Exception();
    }
}

bool Image::save(QString& filename) const {
    return imwrite(filename.toStdString(), this->mat);
}



