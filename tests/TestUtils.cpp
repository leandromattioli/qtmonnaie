/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TestUtils.cpp
 * Author: hulk
 * 
 * Created on November 28, 2016, 2:10 AM
 */

#include "TestUtils.h"
#include "Utils.h"
#include <algorithm>
#include <vector>
#include <string>

std::vector<std::string> TestUtils::getOrderedFiles(std::string path, bool fullpath) {
    std::vector<std::string> entries = Utils::listDir(path, fullpath);
    std::sort(entries.begin(), entries.end());
    return entries;
}
