/* 
 * File:   TestClassify.cpp
 * Author: leandro
 * 
 * Created on 28 de Novembro de 2016, 07:58
 */

#include "TestClassify.h"
#include "Utils.h"
#include "LVQNetwork.h"
#include "PatternTable.h"
#include "CircleImage.h"
#include "constants.h"

#include <cstdio>
#include <iostream>

CPPUNIT_TEST_SUITE_REGISTRATION(TestClassify);

TestClassify::TestClassify() : network(3, 4) {
    path = Paths::Alternate::UnitTestSet2;
    entries = Utils::listDir(path, true, true);
    pat = CircleImage::createPatternTable(entries);
}

TestClassify::~TestClassify() {
}

void TestClassify::setUp() {
}

void TestClassify::tearDown() {
}

void TestClassify::testInitWeights() {
    FILE* log = fopen("log/testInitWeights.txt", "w");
    network.initWeights(pat);
    
    int errors = 0;
    for(int e = 0; e < pat.size(); e++) {
        uint8_t cluster = network.classify(pat[e].first);
        uint8_t target = pat[e].second;
        fprintf(log, "%s %hu %hu\n", entries[e].c_str(), cluster, target);
        CPPUNIT_ASSERT(cluster == target);
    }
    fclose(log);
}