/** 
 * @file testFeatures.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date November 16, 2016, 4:42 PM
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <chrono>
#include <cstdio>
#include <cstdint>
#include <opencv2/core/core.hpp>

#include "config.h"
#include "constants.h"
#include "CircleImage.h"
#include "CircleFeatures.h"
#include "Utils.h"
#include "LVQNetwork.h"
#include "Chronometer.h"
#include "ProgressBar.h"

#ifdef __ARM_NEON
#include <arm_neon.h>
#endif

#ifdef NEON_AVG_COLOR
#define AVG_TYPE "_NEON"
#else
#define AVG_TYPE "_LOOP"
#endif

#ifdef NEON_NORMALIZE
#define NORMALIZE_TYPE "_NEON"
#else
#define NORMALIZE_TYPE "_LOOP"
#endif

#ifdef NEON_CLASSIFY
#define CLASSIFY_TYPE "_NEON"
#else
#define CLASSIFY_TYPE "_LOOP"
#endif


using std::vector;
using std::string;

Chronometer chrono;
LVQNetwork network(3, 4);
PatternTable trainPat;
std::vector<CircleFeatures> testSet;

unsigned long operationAvgColor() {
    chrono.reset();
    chrono.start();
    string path = Paths::Alternate::TrainSet;
    auto entries = Utils::listDir(path, true, true);
    for (auto entry : entries) {
        CircleImage img(entry);
        cv::Vec3b avgColor = img.getAvgColor(0, 1);
    }
    return chrono.pause();
}

std::vector<CircleFeatures> prepareNormalize() {
    vector<CircleFeatures> profileTestSet;
    string path = Paths::Alternate::TrainSet;
    vector<string> entries = Utils::listDir(path, true, true);
    float* inputPattern;
    for (string entry : entries) {
        CircleImage img(entry);
        CircleFeatures features = img.getFeatures();
        profileTestSet.push_back(features);
    }
    return profileTestSet;
}

unsigned long operationNormalize() {
    chrono.reset();
    chrono.start();
    for (auto features : testSet) {
        auto pat = features.toLVQInput();
    }
    return chrono.pause();
}

unsigned long operationClassify() {
    chrono.reset();
    for (uint16_t e = 0; e < trainPat.size(); e++) {
        vector<float> patInput = trainPat[e].first;
        chrono.start();
        uint8_t cluster = network.classify(patInput);
        chrono.pause();
    }
    return chrono.getEllapsed();
}


void profile(const char* name, unsigned count, unsigned long (*fn)(void)) {
    unsigned long ellapsed;
    unsigned i;

    ellapsed = 0;
    for (i = 0; i < count; i++) {
        ellapsed += operationAvgColor();
    }
    printf("%s %u %lu\n", name, i, ellapsed);
}


int main(int argc, char** argv) {
    vector<string> entries;
    entries = Utils::listDir(Paths::Alternate::InitSet, true, true);
    network.initWeights(CircleImage::createPatternTable(entries));

    entries = Utils::listDir(Paths::Alternate::TrainSet, true, true);
    trainPat = CircleImage::createPatternTable(entries);
    testSet = prepareNormalize();
    
    printf("TestName TestRunCount EllapsedTime\n");
    profile("AvgColor" AVG_TYPE, 10, operationAvgColor);
    profile("Normalize" NORMALIZE_TYPE, 100, operationNormalize);
    profile("Classify" CLASSIFY_TYPE, 10, operationClassify);

    return (EXIT_SUCCESS);
}

