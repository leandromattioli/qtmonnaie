/* 
 * File:   TestClassify.h
 * Author: leandro
 *
 * Created on 28 de Novembro de 2016, 07:58
 */

#ifndef TESTCLASSIFY_H
#define TESTCLASSIFY_H

#include <cppunit/extensions/HelperMacros.h>
#include <vector>
#include <string>
#include "LVQNetwork.h"

class TestClassify : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestClassify);
    CPPUNIT_TEST(testInitWeights);
    CPPUNIT_TEST_SUITE_END();

public:
    TestClassify();
    virtual ~TestClassify();
    void setUp();
    void tearDown();

private:
    LVQNetwork network;
    PatternTable pat;
    std::vector<std::string> entries;
    std::string path;
    void testInitWeights();
    
    
};

#endif /* TESTCLASSIFY_H */

