/**
 * @file TestAvgColor.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date Nov 27, 2016, 2:54:21 PM
 */

#include "TestAvgColor.h"
#include "Utils.h"

#include <cstdio>
#include <cstdint>
#include <algorithm>
#include <opencv2/core/core.hpp>
#include "CircleImage.h"

CPPUNIT_TEST_SUITE_REGISTRATION(TestAvgColor);

#define VEC3_FORMAT "%s %hu %hu %hu\n"

using std::string;

void printVector3B(FILE* out, string name, cv::Vec3b vec) {
    fprintf(out, VEC3_FORMAT, name.c_str(), vec[0], vec[1], vec[2]);
}

TestAvgColor::TestAvgColor() {
    path1 = Paths::Alternate::UnitTestSet1;
    path2 = Paths::Alternate::UnitTestSet2;
    entries = Utils::listDir(path2, true, true);
}

TestAvgColor::~TestAvgColor() {
}

void TestAvgColor::setUp() {
}

void TestAvgColor::tearDown() {
}

void TestAvgColor::testAvgColor1() {
    FILE* log = fopen("log/testAvgColor1.txt", "w");
    string filename = "TestAvgColor.bmp";
    string completePath = path1 + "/" + filename;
    CircleImage img(completePath);
    cv::Vec3b avgColor = img.getAvgColor(0, 1);
    printVector3B(log, filename, avgColor);
    CPPUNIT_ASSERT(avgColor[0] == 59);
    CPPUNIT_ASSERT(avgColor[1] == 186);
    CPPUNIT_ASSERT(avgColor[2] == 95);
    fclose(log);
}

void TestAvgColor::testAvgColor2() {
    FILE* log = fopen("log/testAvgColor2.txt", "w");
    const uint8_t results[6][3] = {
        {101, 129, 127}, //cluster 0
        {103, 145, 113},
        {102, 145, 102},
        {103, 142, 99},
        {101, 128, 127}, 
        {101, 137, 113}  //cluster 5
    };
    for (int e = 0; e < entries.size(); e++) {
        string entry = entries[e];
        CircleImage img(entry);
        cv::Vec3b avgColor = img.getAvgColor(0, 1);
        printVector3B(log, entry, avgColor);
        CPPUNIT_ASSERT(avgColor[0] == results[e][0]);
        CPPUNIT_ASSERT(avgColor[1] == results[e][1]);
        CPPUNIT_ASSERT(avgColor[2] == results[e][2]);
    }
    fclose(log);
}



