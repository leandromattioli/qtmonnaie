/** 
 * @file TestFeatures.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * 
 * @date November 28, 2016, 2:00 AM
 */

#include "TestFeatures.h"
#include "Utils.h"

#include "CircleImage.h"
#include "Utils.h"
#include "constants.h"
#include <cstdio>
#include <iostream>
#include <vector>

CPPUNIT_TEST_SUITE_REGISTRATION(TestFeatures);

void printFeatures(FILE* out, const char* name, std::array<uint8_t, 5> arr) {
    fprintf(out, "%s ", name);
    for(int i = 0; i < arr.size() - 1; i++)
        fprintf(out, "%hu ", arr[i]);
    fprintf(out, "%hu\n", arr[arr.size() - 1]);
}

void printFeatures(FILE* out, const char* name, uint16_t* vec) {
    fprintf(out, "%s ", name);
    for(int i = 0; i < 4; i++)
        fprintf(out, "%hu ", vec[i]);
    fprintf(out, "%hu\n", vec[4]);
}

TestFeatures::TestFeatures() {
    path = Paths::Alternate::UnitTestSet2;
    entries = Utils::listDir(path, true, true);
}

TestFeatures::~TestFeatures() {
}

void TestFeatures::setUp() {
}

void TestFeatures::tearDown() {
}

void TestFeatures::testFeatures() {
    uint8_t results[6][5] = {
        {129, 127, 41, 1, 2}, //cluster 0
        {145, 113, 44, 3, 1},
        {145, 102, 41, 0, 3},
        {142, 99, 51, 2, 7},
        {128, 127, 48, 0, 2},
        {137, 113, 56, 17, 32} //cluster 5
    };
    FILE* log = fopen("log/testFeatures.txt", "w");
    for (int e = 0; e < entries.size(); e++) {
        std::string entry = entries[e];
        CircleImage img(entry);
        auto arr = img.getFeatures().toStdArray();
        printFeatures(log, entry.c_str(), arr);
        for (int i = 0; i < arr.size(); i++)
            CPPUNIT_ASSERT(arr[i] == results[e][i]);
    }
    fclose(log);
}